package saiflimited.com.vlc;

import android.content.Context;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.Toast;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.util.ArrayList;

public class VLCFragment extends Fragment implements IVLCVout.Callback {

    private static final String TAG = "PlayerFragment";
    private static final String SAMPLE_URL = "rtsp://test1:i467r5e5y@93.55.193.206:10554/axis-media/media.amp";

    private SurfaceView mVideoSurface = null;

    private LibVLC mLibVLC = null;
    private MediaPlayer mMediaPlayer = null;

    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = container.getContext();
        return inflater.inflate(R.layout.fragment_vlc, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ArrayList<String> args = new ArrayList<>();
        args.add("-vvv");
        args.add("--fullscreen");
        mLibVLC = new LibVLC(context, args);
        mMediaPlayer = new MediaPlayer(mLibVLC);
        mMediaPlayer.setVideoTrackEnabled(true);
        final MediaController mediaController = new MediaController(getActivity());
        Button go = view.findViewById(R.id.submit);
        final  EditText urlTv = view.findViewById(R.id.url);
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = urlTv.getText().toString();
                if(!url.isEmpty()) {
                    if(mMediaPlayer!=null && mMediaPlayer.isPlaying()) {
                        mMediaPlayer.stop();
                        mMediaPlayer.getVLCVout().detachViews();
                        mMediaPlayer.getVLCVout().removeCallback(VLCFragment.this);
                    }
                    final IVLCVout vlcVout = mMediaPlayer.getVLCVout();
                    vlcVout.setVideoView(mVideoSurface);
                    vlcVout.attachViews();
                    mMediaPlayer.getVLCVout().addCallback(VLCFragment.this);
                    Media media = new Media(mLibVLC, Uri.parse(SAMPLE_URL));
                    mMediaPlayer.setMedia(media);
                    media.release();
                    mMediaPlayer.play();
                    setSize();
                } else {
                    Toast.makeText(getContext(),"Please enter urls",Toast.LENGTH_LONG).show();
                }

            }
        });
        mVideoSurface = (SurfaceView) view.findViewById(R.id.video);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);



        mMediaPlayer.setEventListener(new MediaPlayer.EventListener() {
            @Override
            public void onEvent(MediaPlayer.Event event) {
                switch (event.type){
                    case MediaPlayer.Event.Buffering:
                        Log.d(TAG, "onEvent: Buffering");
                        break;
                    case MediaPlayer.Event.EncounteredError:
                        Log.d(TAG, "onEvent: EncounteredError");
                        break;
                    case MediaPlayer.Event.EndReached:
                        Log.d(TAG, "onEvent: EndReached");
                        break;
                    case MediaPlayer.Event.ESAdded:
                        Log.d(TAG, "onEvent: ESAdded");
                        break;
                    case MediaPlayer.Event.ESDeleted:
                        Log.d(TAG, "onEvent: ESDeleted");
                        break;  case MediaPlayer.Event.MediaChanged:
                        Log.d(TAG, "onEvent: MediaChanged");
                        break;
                    case MediaPlayer.Event.Opening:
                        Log.d(TAG, "onEvent: Opening");
                        break;
                    case MediaPlayer.Event.PausableChanged:
                        Log.d(TAG, "onEvent: PausableChanged");
                        break;
                    case MediaPlayer.Event.Paused:
                        Log.d(TAG, "onEvent: Paused");
                        break;
                    case MediaPlayer.Event.Playing:
                        Log.d(TAG, "onEvent: Playing");
                        break;
                    case MediaPlayer.Event.PositionChanged:
//                        Log.d(TAG, "onEvent: PositionChanged");
                        break;
                    case MediaPlayer.Event.SeekableChanged:
                        Log.d(TAG, "onEvent: SeekableChanged");
                        break;
                    case MediaPlayer.Event.Stopped:
                        Log.d(TAG, "onEvent: Stopped");
                        break;
                    case MediaPlayer.Event.TimeChanged:
//                        Log.d(TAG, "onEvent: TimeChanged");
                        break;
                    case MediaPlayer.Event.Vout:
                        Log.d(TAG, "onEvent: Vout");
                        break;
                }
            }
        });
    }


    private void setSize() {
        // get screen size
        int w = getActivity().getWindow().getDecorView().getWidth();
        int h = getActivity().getWindow().getDecorView().getHeight();

        // getWindow().getDecorView() doesn't always take orientation into
        // account, we have to correct the values
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (w > h && isPortrait || w < h && !isPortrait) {
            int i = w;
            w = h;
            h = i;
        }


        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        ViewGroup.LayoutParams lp = mVideoSurface.getLayoutParams();
        lp.width = width;
        lp.height=height;

        mVideoSurface.setLayoutParams(lp);
        mVideoSurface.invalidate();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mMediaPlayer.release();
        mLibVLC.release();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMediaPlayer.stop();
        mMediaPlayer.getVLCVout().detachViews();
        mMediaPlayer.getVLCVout().removeCallback(this);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {
    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {
    }
}
