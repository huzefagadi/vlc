package saiflimited.com.vlc; /*****************************************************************************
 * JavaActivity.java
 *****************************************************************************
 * Copyright (C) 2016 VideoLAN
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD license. See the LICENSE file for details.
 *****************************************************************************/


import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.VideoView;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IVLCVout.OnNewVideoLayoutListener {
    private static final String TAG = "JavaActivity";
    //private static final String SAMPLE_URL = "http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_640x360.m4v";

    private static final int SURFACE_BEST_FIT = 0;
    private static final int SURFACE_FIT_SCREEN = 1;
    private static final int SURFACE_FILL = 2;
    private static final int SURFACE_16_9 = 3;
    private static final int SURFACE_4_3 = 4;
    private static final int SURFACE_ORIGINAL = 5;
    private static final String SAMPLE_URL = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov";
    private static final String SAMPLE_URL_2 = "http://test1:i467r5e5y@93.55.193.206:1080/mjpg/video.mjpg";
    private static final String SAMPLE_URL_3 = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov";
    private static final String SAMPLE_URL_4 = "rtsp://test1:i467r5e5y@93.55.193.206:10554/axis-media/media.amp";
    private static int CURRENT_SIZE = SURFACE_FILL;

    private FrameLayout mVideoSurfaceFrame1 = null;
    private SurfaceView mVideoSurface1 = null;
    private View mVideoView1 = null;
    private FrameLayout mVideoSurfaceFrame2 = null;
    private SurfaceView mVideoSurface2 = null;
    private View mVideoView2 = null;
    private final Handler mHandler1 = new Handler();
    private final Handler mHandler2 = new Handler();
    private View.OnLayoutChangeListener mOnLayoutChangeListener1 = null;
    private View.OnLayoutChangeListener mOnLayoutChangeListener2 = null;
    private LibVLC mLibVLC1 = null;
    private LibVLC mLibVLC2 = null;
    private MediaPlayer mMediaPlayer1 = null;
    private MediaPlayer mMediaPlayer2 = null;


    private FrameLayout mVideoSurfaceFrame3 = null;
    private SurfaceView mVideoSurface3 = null;
    private View mVideoView3 = null;
    private FrameLayout mVideoSurfaceFrame4 = null;
    private SurfaceView mVideoSurface4 = null;
    private View mVideoView4 = null;
    private final Handler mHandler3 = new Handler();
    private final Handler mHandler4 = new Handler();
    private View.OnLayoutChangeListener mOnLayoutChangeListener3 = null;
    private View.OnLayoutChangeListener mOnLayoutChangeListener4 = null;
    private LibVLC mLibVLC3 = null;
    private LibVLC mLibVLC4 = null;
    private MediaPlayer mMediaPlayer3 = null;
    private MediaPlayer mMediaPlayer4 = null;


    private int mVideoHeight = 0;
    private int mVideoWidth = 0;
    private int mVideoVisibleHeight = 0;
    private int mVideoVisibleWidth = 0;
    private int mVideoSarNum = 0;
    private int mVideoSarDen = 0;
    final ArrayList<String> args = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        args.add("-vvv");
        args.add("--file-caching=1500");
        args.add("--drop-late-frames");

        initilizeViews();

        initializePlayers();
    }

    private void initilizeViews() {
        mVideoSurfaceFrame1 = (FrameLayout) findViewById(R.id.video_surface_frame_1);
        ViewStub stub = (ViewStub) findViewById(R.id.surface_stub_1);
        mVideoSurface1 = (SurfaceView) stub.inflate();


        mVideoSurfaceFrame2 = (FrameLayout) findViewById(R.id.video_surface_frame_2);
        ViewStub stub2 = (ViewStub) findViewById(R.id.surface_stub_2);
        mVideoSurface2 = (SurfaceView) stub2.inflate();

        mVideoSurfaceFrame3 = (FrameLayout) findViewById(R.id.video_surface_frame_3);
        ViewStub stub3 = (ViewStub) findViewById(R.id.surface_stub_3);
        mVideoSurface3 = (SurfaceView) stub3.inflate();


        mVideoSurfaceFrame4 = (FrameLayout) findViewById(R.id.video_surface_frame_4);
        ViewStub stub4 = (ViewStub) findViewById(R.id.surface_stub_4);
        mVideoSurface4 = (SurfaceView) stub4.inflate();

    }

    private void initializePlayers() {
        mLibVLC1 = new LibVLC(this, args);
        mMediaPlayer1 = new MediaPlayer(mLibVLC1);
        mVideoView1 = mVideoSurface1;


        mLibVLC2 = new LibVLC(this, args);
        mMediaPlayer2 = new MediaPlayer(mLibVLC2);
        mVideoView2 = mVideoSurface2;

        mLibVLC3 = new LibVLC(this, args);
        mMediaPlayer3 = new MediaPlayer(mLibVLC3);
        mVideoView3 = mVideoSurface3;


        mLibVLC4 = new LibVLC(this, args);
        mMediaPlayer4 = new MediaPlayer(mLibVLC4);
        mVideoView4 = mVideoSurface4;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer(mMediaPlayer1, mLibVLC1);
        releasePlayer(mMediaPlayer2, mLibVLC2);
        releasePlayer(mMediaPlayer3, mLibVLC3);
        releasePlayer(mMediaPlayer4, mLibVLC4);
    }

    private void releasePlayer(MediaPlayer mMediaPlayer1, LibVLC mLibVLC1) {
        mMediaPlayer1.release();
        mLibVLC1.release();
    }

    @Override
    protected void onStart() {
        super.onStart();

        playMedia(mMediaPlayer1, mVideoSurface1, mLibVLC1, SAMPLE_URL, mVideoSurfaceFrame1, mVideoView1, mOnLayoutChangeListener1, mHandler1);
        playMedia(mMediaPlayer2, mVideoSurface2, mLibVLC2, SAMPLE_URL_2, mVideoSurfaceFrame2, mVideoView2, mOnLayoutChangeListener2, mHandler2);
        playMedia(mMediaPlayer3, mVideoSurface3, mLibVLC3, SAMPLE_URL_3, mVideoSurfaceFrame3, mVideoView3, mOnLayoutChangeListener3, mHandler3);
        playMedia(mMediaPlayer4, mVideoSurface4, mLibVLC4, SAMPLE_URL_4, mVideoSurfaceFrame4, mVideoView4, mOnLayoutChangeListener4, mHandler4);
    }

    private synchronized void playMedia(final MediaPlayer mMediaPlayer1,
                           SurfaceView mVideoSurface1,
                           LibVLC mLibVLC1,
                           String url,
                           final FrameLayout mVideoSurfaceFrame1,
                           final View mVideoView1,
                           View.OnLayoutChangeListener mOnLayoutChangeListener1,
                           final Handler mHandler1) {
        final IVLCVout vlcVout = mMediaPlayer1.getVLCVout();
        if (mVideoSurface1 != null) {
            vlcVout.setVideoView(mVideoSurface1);
        }
        vlcVout.attachViews(this);

        Media media = new Media(mLibVLC1, Uri.parse(url));
        mMediaPlayer1.setMedia(media);
        media.release();
        mMediaPlayer1.play();

        if (mOnLayoutChangeListener1 == null) {
            mOnLayoutChangeListener1 = new View.OnLayoutChangeListener() {
                private final Runnable mRunnable1 = new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "updateVideoSurfaces: ");
                        updateVideoSurfaces(mVideoView1, mVideoSurfaceFrame1, mMediaPlayer1);
                    }
                };

                @Override
                public void onLayoutChange(View v, int left, int top, int right,
                                           int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    Log.d(TAG, "onLayoutChange: ");
                    if (left != oldLeft || top != oldTop || right != oldRight || bottom != oldBottom) {
                        mHandler1.removeCallbacks(mRunnable1);
                        mHandler1.post(mRunnable1);
                    }
                }
            };
        }
        mVideoSurfaceFrame1.addOnLayoutChangeListener(mOnLayoutChangeListener1);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopVideos(mOnLayoutChangeListener1, mVideoSurfaceFrame1, mMediaPlayer1);
        stopVideos(mOnLayoutChangeListener2, mVideoSurfaceFrame2, mMediaPlayer2);
        stopVideos(mOnLayoutChangeListener3, mVideoSurfaceFrame3, mMediaPlayer3);
        stopVideos(mOnLayoutChangeListener4, mVideoSurfaceFrame4, mMediaPlayer4);
    }

    private synchronized void stopVideos(View.OnLayoutChangeListener mOnLayoutChangeListener1,
                            FrameLayout mVideoSurfaceFrame1, MediaPlayer mMediaPlayer1) {
        if (mOnLayoutChangeListener1 != null) {
            mVideoSurfaceFrame1.removeOnLayoutChangeListener(mOnLayoutChangeListener1);
            mOnLayoutChangeListener1 = null;
        }

        mMediaPlayer1.stop();
        mMediaPlayer1.getVLCVout().detachViews();
    }

    private synchronized void changeMediaPlayerLayout(MediaPlayer mMediaPlayer1, int displayW, int displayH) {
        /* Change the video placement using the MediaPlayer API */
        switch (CURRENT_SIZE) {
            case SURFACE_BEST_FIT:
                mMediaPlayer1.setAspectRatio(null);
                mMediaPlayer1.setScale(0);
                break;
            case SURFACE_FIT_SCREEN:
            case SURFACE_FILL: {
                Media.VideoTrack vtrack = mMediaPlayer1.getCurrentVideoTrack();
                if (vtrack == null)
                    return;
                final boolean videoSwapped = vtrack.orientation == Media.VideoTrack.Orientation.LeftBottom
                        || vtrack.orientation == Media.VideoTrack.Orientation.RightTop;
                if (CURRENT_SIZE == SURFACE_FIT_SCREEN) {
                    int videoW = vtrack.width;
                    int videoH = vtrack.height;

                    if (videoSwapped) {
                        int swap = videoW;
                        videoW = videoH;
                        videoH = swap;
                    }
                    if (vtrack.sarNum != vtrack.sarDen)
                        videoW = videoW * vtrack.sarNum / vtrack.sarDen;

                    float ar = videoW / (float) videoH;
                    float dar = displayW / (float) displayH;

                    float scale;
                    if (dar >= ar)
                        scale = displayW / (float) videoW; /* horizontal */
                    else
                        scale = displayH / (float) videoH; /* vertical */
                    mMediaPlayer1.setScale(scale);
                    mMediaPlayer1.setAspectRatio(null);
                } else {
                    mMediaPlayer1.setScale(0);
                    mMediaPlayer1.setAspectRatio(!videoSwapped ? "" + displayW + ":" + displayH
                            : "" + displayH + ":" + displayW);
                }
                break;
            }
            case SURFACE_16_9:
                mMediaPlayer1.setAspectRatio("16:9");
                mMediaPlayer1.setScale(0);
                break;
            case SURFACE_4_3:
                mMediaPlayer1.setAspectRatio("4:3");
                mMediaPlayer1.setScale(0);
                break;
            case SURFACE_ORIGINAL:
                mMediaPlayer1.setAspectRatio(null);
                mMediaPlayer1.setScale(1);
                break;
        }
    }

    private synchronized void updateVideoSurfaces(View mVideoView1, FrameLayout mVideoSurfaceFrame1, MediaPlayer mMediaPlayer1) {
        int sw = getWindow().getDecorView().getWidth() / 2;
        int sh = getWindow().getDecorView().getHeight() / 2;

        // sanity check
        if (sw * sh == 0) {
            Log.e(TAG, "Invalid surface size");
            return;
        }

        mMediaPlayer1.getVLCVout().setWindowSize(sw, sh);

        ViewGroup.LayoutParams lp = mVideoView1.getLayoutParams();
        if (mVideoWidth * mVideoHeight == 0) {
            /* Case of OpenGL vouts: handles the placement of the video using MediaPlayer API */
            lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoView1.setLayoutParams(lp);
            lp = mVideoSurfaceFrame1.getLayoutParams();
            lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoSurfaceFrame1.setLayoutParams(lp);
            changeMediaPlayerLayout(mMediaPlayer1, sw, sh);
            return;
        }

        if (lp.width == lp.height && lp.width == ViewGroup.LayoutParams.MATCH_PARENT) {
            /* We handle the placement of the video using Android View LayoutParams */
            mMediaPlayer1.setAspectRatio(null);
            mMediaPlayer1.setScale(0);
        }

        double dw = sw, dh = sh;
        final boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

        if (sw > sh && isPortrait || sw < sh && !isPortrait) {
            dw = sh;
            dh = sw;
        }

        // compute the aspect ratio
        double ar, vw;
        if (mVideoSarDen == mVideoSarNum) {
            /* No indication about the density, assuming 1:1 */
            vw = mVideoVisibleWidth;
            ar = (double) mVideoVisibleWidth / (double) mVideoVisibleHeight;
        } else {
            /* Use the specified aspect ratio */
            vw = mVideoVisibleWidth * (double) mVideoSarNum / mVideoSarDen;
            ar = vw / mVideoVisibleHeight;
        }

        // compute the display aspect ratio
        double dar = dw / dh;

        switch (CURRENT_SIZE) {
            case SURFACE_BEST_FIT:
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_FIT_SCREEN:
                if (dar >= ar)
                    dh = dw / ar; /* horizontal */
                else
                    dw = dh * ar; /* vertical */
                break;
            case SURFACE_FILL:
                break;
            case SURFACE_16_9:
                ar = 16.0 / 9.0;
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_4_3:
                ar = 4.0 / 3.0;
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_ORIGINAL:
                dh = mVideoVisibleHeight;
                dw = vw;
                break;
        }

        // set display size
        lp.width = (int) Math.ceil(dw * mVideoWidth / mVideoVisibleWidth);
        lp.height = (int) Math.ceil(dh * mVideoHeight / mVideoVisibleHeight);
        mVideoView1.setLayoutParams(lp);

        // set frame size (crop if necessary)
        lp = mVideoSurfaceFrame1.getLayoutParams();
        lp.width = (int) Math.floor(dw);
        lp.height = (int) Math.floor(dh);
        mVideoSurfaceFrame1.setLayoutParams(lp);

        mVideoView1.invalidate();
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onNewVideoLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        mVideoWidth = width;
        mVideoHeight = height;
        mVideoVisibleWidth = visibleWidth;
        mVideoVisibleHeight = visibleHeight;
        mVideoSarNum = sarNum;
        mVideoSarDen = sarDen;
        Log.d(TAG, "onNewVideoLayout: ");
        updateVideoSurfaces(mVideoView1, mVideoSurfaceFrame1, mMediaPlayer1);
        updateVideoSurfaces(mVideoView2, mVideoSurfaceFrame2, mMediaPlayer2);
        updateVideoSurfaces(mVideoView3, mVideoSurfaceFrame3, mMediaPlayer3);
        updateVideoSurfaces(mVideoView4, mVideoSurfaceFrame4, mMediaPlayer4);
    }


}